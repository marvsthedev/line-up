# To get dev env up (api, frontend and db)
`docker-compose up --build` or `docker-compose up`


# UI
You should now be able to reach the frontend by 'http://localhost:1234/'

To get a user 'http://localhost:1234/user/{id}'


# API
You can reach the backend by 'http://localhost:5000/'

To get a user 'http://localhost:5000/api/v1/user/{id}'
