"""
Module for some helpful utilities.
"""
import re

snake_case = re.compile(r'_([a-z])')


def snake_case_to_camel(name):
    """Converts name from snake case to camel case."""
    return snake_case.sub(lambda x: x.group(1).upper(), name)


def convert_json(old_dict, convert_method):
    """
    Applies convert_method to keys from the old_dict
    and creates a new dict with the converted keys.
    """
    new_dict = {}
    for k, v in old_dict.items():
        new_dict[convert_method(k)] = convert_json(v, convert_method) if isinstance(v, dict) else v

    return new_dict
