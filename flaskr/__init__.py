"""
Main module which is in charge of getting the right config and define how the flask app should be created.
"""

import os
import requests

from flask import Flask, jsonify
from flask_cors import CORS
from flask_migrate import Migrate

from .models import db
from .schema import ma, user_schema
from .utils import snake_case_to_camel, convert_json


def load_config():
    """Load config dependent on what MODE set i.e. load StagingConfig if MODE=staging."""
    mode = os.environ.get('MODE')
    if mode == 'production':
        from .config import ProductionConfig
        return ProductionConfig()
    elif mode == 'staging':
        from .config import StagingConfig
        return StagingConfig()
    elif mode == 'development':
        from .config import DevelopmentConfig
        return DevelopmentConfig()
    elif mode == 'testing':
        from .config import TestingConfig
        return TestingConfig()
    
    raise Exception('You need to specify a mode')


def create_app():
    """Creates and configure the app."""
    app = Flask(__name__, instance_relative_config=True)
    config = load_config()
    app.config.from_object(config)
    db.init_app(app)
    Migrate(app, db)
    ma.init_app(app)
    CORS(app)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route("/api/v1/user/<int:user_id>")
    def get_user(user_id: str):
        """View to get a singular user."""
        response = requests.get(f'https://reqres.in/api/users/{user_id}')

        # Parnoid me has done this in the event the response from requests does not have a 'data' key
        user = response.json().get('data', None)
        result = user_schema.dump(user)
        converted_results = convert_json(result, snake_case_to_camel)
        return jsonify(converted_results)

    return app
