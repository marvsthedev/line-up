"""
Module for all our marshmallow schemas.
"""
from flask_marshmallow import Marshmallow

ma = Marshmallow()


class UserSchema(ma.Schema):
    """User Schema."""
    id = ma.Int()
    email = ma.Email()
    first_name = ma.Str()
    last_name = ma.Str()
    avatar = ma.Url()

user_schema = UserSchema()
