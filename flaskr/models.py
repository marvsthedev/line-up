"""
Module for all our models.
"""
from flask_sqlalchemy import SQLAlchemy
from passlib.context import CryptContext

db = SQLAlchemy()
PASSLIB_CONTEXT = CryptContext(
    schemes=['pbkdf2_sha512'],
    deprecated='auto',
)


def get_or_create(session, model, **kwargs):
    """get_or_create helper function inspired by Django's own get_or_create."""
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance, True


class PasswordGuard(db.Model):
    """This class will add password features to any model that inherits from it."""
    __abstract__: bool = True

    password_hash = db.Column(db.String(255), nullable=False)

    def __init__(self, password=None, password_hash=None, **kwargs):
        """
        We set the password_has on a model that is inheriting Password Guard upon initialisation. An error will
        occour if one attempts to create a inheriting model without passing a value to password. This is because
        password_has is not nullable and will only be set if one passes a value to password.
        """
        if password_hash is None and password is not None:
            password_hash = self.generate_hash(password)

        super().__init__(password_hash=password_hash, **kwargs)

    @property
    def password(self):
        """We don't anyone reading the password including us."""
        raise AttributeError('User.password is write-only')

    @password.setter
    def password(self, password):
        """Hashes the password and sets it to password_has."""
        self.password_hash = self.generate_hash(password)

    def verify_password(self, password):
        """Method to check if password supplied matches the password we have saved."""
        return PASSLIB_CONTEXT.verify(password, self.password_hash)

    @staticmethod
    def generate_hash(password):
        """Generate a secure password hash from a new password"""
        return PASSLIB_CONTEXT.hash(password.encode('utf'))


class User(PasswordGuard):
    """User model."""
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    first_name = db.Column(db.String(255), nullable=False)
    last_name = db.Column(db.String(255), nullable=False)
    avatar = db.Column(db.String(255))

    def __repr__(self):
        """Retutns a printable representation of the user."""
        return f'<User({self.fullname})>'

    @property
    def fullname(self):
        """Returns the fullname of a user by combining the first and last name."""
        return f'{self.first_name} {self.last_name}'