"""
Module for all our configs.
"""
class Config(object):
    """Base config."""
    # SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and will be disabled by default in the future.
    # Set it to True to suppress this warning.
    SQLALCHEMY_TRACK_MODIFICATIONS: bool = False


class ProductionConfig(Config):
    """Production config."""
    DEBUG: bool = False
    TESTING: bool = False


class StagingConfig(Config):
    """Staging config."""
    DEBUG: bool = False
    TESTING: bool = False


class DevelopmentConfig(Config):
    """Development config."""
    DEBUG: bool = True
    TESTING: bool = False
    SQLALCHEMY_DATABASE_URI: str = 'postgresql://line_up:line_up@localdb/line_up'


class TestingConfig(Config):
    """Testing config."""
    DEBUG: bool = False
    TESTING: bool = True
