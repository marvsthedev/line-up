import React from 'react';
import { render } from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import User from './user';
import '../scss/index.scss';

function App () {
  return (
    <Router>
      {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/user/:id" component={User} />
      </Switch>
    </Router>
  );
}

render(<App />, document.getElementById('root'));
