import React from 'react';
import { RouteComponentProps } from 'react-router';

async function http<T> (url: string): Promise<T> {
  return fetch(url)
    .then(response => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json() as Promise<T>;
    });
}

interface MatchParams {
  id: string;
}

interface Props extends RouteComponentProps<MatchParams> {
}

interface IUser {
  id: string,
  firstName: string,
  lastName: string,
  email: string,
  avatar: string
}

type State = {
  user?: IUser
};

export default class User extends React.Component<Props, State> {
  state: State = {
    user: undefined
  };

  public componentDidMount = async (): Promise<void> => {
    const data = await http<IUser>(`http://127.0.0.1:5000/api/v1/user/${this.props.match.params.id}`);
    this.setState({ user: data });
  }

  public render (): React.ReactNode {
    return (
      <div>
        <h1>Application</h1>
        {this.state.user &&
          <div>
            <p>id: {this.state.user.id}</p>
            <p>first_name: {this.state.user.firstName}</p>
            <p>last_name: {this.state.user.lastName}</p>
            <p>email: {this.state.user.email}</p>
            <p>avatar: {this.state.user.avatar}</p>
          </div>
        }
      </div>
    );
  }
}
