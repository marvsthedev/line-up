-r requirements.txt
ipython==7.18.1
pdbpp==0.10.2
flake8==3.8.3
flask-shell-ipython==0.4.1
mypy==0.782
pylint==2.6.0
pytest==6.0.2
pytest-cov==2.10.1
pytest-flask==1.0.0
tox==3.20.0
